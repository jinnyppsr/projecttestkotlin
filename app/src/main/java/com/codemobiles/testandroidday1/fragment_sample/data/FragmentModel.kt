package com.codemobiles.testandroidday1.fragment_sample.data

import androidx.fragment.app.Fragment

// didn't use data class because data help to do compare()/ copy()
class FragmentModel (
    val tabName: String,
    val fragment: Fragment
)