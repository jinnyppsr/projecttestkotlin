package com.codemobiles.testandroidday1.fragment_sample.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.codemobiles.testandroidday1.fragment_sample.data.FragmentModel

// Adapter for 2 pages
// receive parameter as fmList to show in other page
//private val context: Context,
class SectionsPagerAdapter(
                           val fmList: List<FragmentModel> ,
                           fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getItem(position: Int): Fragment {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
//        return PlaceholderFragment.newInstance(position + 1)
        return fmList[position].fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return  fmList[position].tabName

        // Adapter does'nt have it own resources because it is not Activity class,
        // so it has to call like below. => context.resources.getString()
//        return context.resources.getString(TAB_TITLES[position])
    }

    override fun getCount(): Int {
        // Show 2 total pages.
//        return 2
        return fmList.count()
    }
}