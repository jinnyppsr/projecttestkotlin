package com.codemobiles.testandroidday1.fragment_sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.codemobiles.testandroidday1.R
import com.codemobiles.testandroidday1.fragment_sample.data.FragmentModel
import com.codemobiles.testandroidday1.fragment_sample.ui.main.PlaceholderFragment
import com.codemobiles.testandroidday1.fragment_sample.ui.main.ProfileFragment
import com.codemobiles.testandroidday1.fragment_sample.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setView()

    }

    private fun setView() {
        val tabList: List<FragmentModel> = listOf<FragmentModel>(
            FragmentModel("Home", PlaceholderFragment.newInstance(1)),
            FragmentModel("Profile", ProfileFragment.newInstance())
        )

        val sectionsPagerAdapter = SectionsPagerAdapter( tabList, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }

//    private fun getFragmentModel(name: String, fragment: Fragment) : FragmentModel {
//        return FragmentModel(name, fragment)
//    }
}