package com.codemobiles.testandroidday1.todo

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.CompoundButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.codemobiles.testandroidday1.R
import com.codemobiles.testandroidday1.todo.model.ToDoListModel

class TodoListViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
    LayoutInflater.from(parent.context).inflate(R.layout.item_todo, parent, false)
){
    private val taskName: TextView = itemView.findViewById(R.id.taskText)
    private val checkBoxItem: CheckBox = itemView.findViewById(R.id.checkBoxItem)

    fun bind(model: ToDoListModel, listener: ToDoClickListener) {
        taskName.text = model.taskName
        itemView.setOnClickListener {
            listener.onItemClick(taskName.text.toString())
        }

        checkBoxItem.setOnCheckedChangeListener(object: CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(p0: CompoundButton?, p1: Boolean) {
                Log.i("onClickCheckBox", "isChecked")
            }

        })

        // check if that check box is already checked, do View.GONE to hide that item.
        itemView.visibility = if (model.isComplete) {
            View.GONE
        } else {
            View.VISIBLE
        }
    }

}