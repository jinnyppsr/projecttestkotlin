package com.codemobiles.testandroidday1.todo

import com.codemobiles.testandroidday1.InClassIntroductionInterface

class IntroductionPresenter(private val view: InClassIntroductionInterface) {

    fun init() {
        val information = "Hello User, Welcome to SCB"
        view.setContent(information)
    }

}