package com.codemobiles.testandroidday1.todo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// use Parcelable with data class because this model's type isn't int/String
// use Parcelize extension instead of override method from Parcel
// Parcelize -> save data and get it out
@Parcelize
data class ToDoListModel
    (var isComplete: Boolean,
     var taskName: String): Parcelable
