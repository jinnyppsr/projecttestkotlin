package com.codemobiles.testandroidday1.todo

interface ToDoClickListener {

    fun onItemClick(taskName: String)

}