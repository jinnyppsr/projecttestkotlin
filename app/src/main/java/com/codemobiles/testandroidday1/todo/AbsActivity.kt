package com.codemobiles.testandroidday1.todo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.codemobiles.testandroidday1.InClassIntroductionInterface
import kotlinx.android.synthetic.main.activity_main.*


abstract class AbsActivity : AppCompatActivity(), InClassIntroductionInterface {

    abstract fun navigateNext()
    abstract fun getChildLayout(): Int

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
        setContentView(getChildLayout())
        setView()
    }

    private fun setView() {
        setContent("Hello User, Welcome to SCB")
        btnNext.setOnClickListener {
            navigateNext()
        }
    }

    override fun setContent(contenText: String) {
        introductionText.text = contenText
    }


}
