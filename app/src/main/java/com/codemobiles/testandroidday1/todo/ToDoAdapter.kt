package com.codemobiles.testandroidday1.todo

import android.util.Log
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.codemobiles.testandroidday1.todo.model.ToDoListModel

class ToDoAdapter(val listener: ToDoClickListener) : RecyclerView.Adapter<TodoListViewHolder>(){
    // val listener because it is not global variable

    private val todoList: ArrayList<ToDoListModel> = arrayListOf()

    // collect the view before
    // return TodoListViewHolder(Listview => parent)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)=
        TodoListViewHolder(parent)

    // ex. setText one by one
    override fun onBindViewHolder(holder: TodoListViewHolder, position: Int) {
        holder.bind(todoList[position], listener)
    }

    // Tell that recyclerView will show how many items.
    override fun getItemCount() = todoList.count()

    fun addListTask(taskModel: ToDoListModel){
        todoList.add(taskModel)
        Log.i("todoList", todoList.toString())
        notifyDataSetChanged()  // Means that all data had changed.
    }

}