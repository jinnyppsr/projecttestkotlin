package com.codemobiles.testandroidday1.todo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.codemobiles.testandroidday1.MainActivity
import com.codemobiles.testandroidday1.R
import com.codemobiles.testandroidday1.todo.model.ToDoListModel
import kotlinx.android.synthetic.main.todo_list.*

class TodoInputActivity : AppCompatActivity() {

    private lateinit var todoAdapter: ToDoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.todo_list)
        setView()
    }

    private fun setView() {
        // declare listener to be object
        val listener = object : ToDoClickListener {
            override fun onItemClick(taskName: String) {
                Log.i("onItemClick: ", "navigate to new screen")
                MainActivity.startActivity(this@TodoInputActivity, taskName)
            }
        }

        todoAdapter = ToDoAdapter(listener)

        rvTodo.adapter = todoAdapter
        rvTodo.layoutManager = LinearLayoutManager(this)
        rvTodo.itemAnimator = DefaultItemAnimator()

        btnAdd.setOnClickListener {
            var name = editText.text.toString()
            todoAdapter.addListTask(getAddModel(name))
            editText.text = null
        }
    }

    private fun getAddModel(name: String): ToDoListModel{
        return ToDoListModel(false, name)
    }

}