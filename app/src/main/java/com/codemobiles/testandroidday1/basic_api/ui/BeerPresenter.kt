package com.codemobiles.testandroidday1.basic_api.ui

import android.util.Log
import com.codemobiles.testandroidday1.basic_api.data.BeerModel
import com.codemobiles.testandroidday1.basic_api.service.BeerManager
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class BeerPresenter(val view: BeerInterface) {

    fun getBeerApi(){
        BeerManager().createService().getRandomBeer().enqueue(object: Callback<List<BeerModel>> {
            override fun onFailure(call: Call<List<BeerModel>>, t: Throwable) {
                Log.i("getApiFail: ", t.message.toString())
            }

            override fun onResponse(call: Call<List<BeerModel>>, response: Response<List<BeerModel>>) {
                response.body()?.apply {
                    if (this.isNotEmpty()) {
                        view.setBeer(this[0])
                    }
                }
            }

        })
    }
}