package com.codemobiles.testandroidday1.basic_api.data

import android.accounts.AuthenticatorDescription
import com.google.gson.annotations.SerializedName

data class BeerModel(
    @SerializedName("name")
    val name: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("abv")
    val abv: Double?,

    @SerializedName("image_url")
    val image_url: String?


)