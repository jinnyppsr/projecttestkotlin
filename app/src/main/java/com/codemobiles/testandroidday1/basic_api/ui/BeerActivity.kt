package com.codemobiles.testandroidday1.basic_api.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.codemobiles.testandroidday1.R
import com.codemobiles.testandroidday1.basic_api.data.BeerModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.*

class BeerActivity : AppCompatActivity(), BeerInterface {

    private val presenter = BeerPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_beer)
        presenter.getBeerApi()
        setView()
    }

    override fun setBeer(beerItem: BeerModel) {
        textName.text = "${beerItem.name}"
        textDescription.text = "${beerItem.description}"
        textAbv.text = "${beerItem.abv} %"
        Log.i("Image type ", "${beerItem.image_url}")
        Picasso.get()
            .load(beerItem.image_url)
            .placeholder(R.drawable.jesus)
            .into(imgBeer)
    }

    private fun setView() {
        btnRefresh.setOnClickListener {
            presenter.getBeerApi()
        }
    }

}
