package com.codemobiles.testandroidday1.basic_api.ui

import com.codemobiles.testandroidday1.basic_api.data.BeerModel

interface BeerInterface {

    fun setBeer(beerItem: BeerModel)

}