package com.codemobiles.testandroidday1

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.codemobiles.testandroidday1.todo.IntroductionPresenter
//import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), InClassIntroductionInterface {

    companion object {
        const val EXTRA_KEY_TITLE = "TITLE_NAME"
        const val EXTRA_KEY_MODEL = "MODEL"
//        fun startActivity(context: Context, titleName: String, model: ToDoListModel) =
        fun startActivity(context: Context, titleName: String) =
                context.startActivity(
                    // Use Intent to communicate between Activity(Page)
                    Intent(context, MainActivity::class.java).also { myIntent ->
                        myIntent.putExtra(EXTRA_KEY_TITLE, titleName)
//                        myIntent.putExtra(EXTRA_KEY_MODEL, model)
                    })
    }

    // this => Class's Character after implement interface Inclass..
    private val presenter = IntroductionPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.init()
        // use getString because R.string.tagName type is int, so it has to change to String
//        val tagName = getString(R.string.tagName)

        title = intent.getStringExtra(EXTRA_KEY_TITLE)

//        val model: ToDoListModel = intent.getParcelableExtra(EXTRA_KEY_MODEL)
//        println("OK 200 > $model")
    }

    // override from Interface name InClassIntroductionInterface
    override fun setContent(contecText: String) {
        // set UI content
    }

    override fun navigateToNextPage() {
        // navigate to next screen
    }
}

interface InClassIntroductionInterface {
    fun setContent(contecText: String)

    fun navigateToNextPage()
}
